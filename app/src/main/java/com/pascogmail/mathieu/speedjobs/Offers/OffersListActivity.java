package com.pascogmail.mathieu.speedjobs.offers;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.pascogmail.mathieu.speedjobs.R;
import com.pascogmail.mathieu.speedjobs.SideMenu;

import java.util.ArrayList;
import java.util.List;

public class OffersListActivity extends SideMenu {

    ListView offersListView;

    //Getting jobs offers :
    private String[] offersListTitles = {
            "Développeur Symfony 2 H/F",
            "Développeur Cobol H/F",
            "Développeur Java H/F",
            "Développeur Java H/F",
            "Administrateur système et réseaux H/F",
            "Développeur Windev H/F",
            "Développeur C# H/F",
            "Développeur PHP H/F",
            "Développeur Laravel H/F"
    };

    private String[] offersListDescriptions = {
            "Rennes - Dolmen Technologies",
            "Rennes - Crédit Agricole d'Ille et Villaine",
            "Laval - Archimaine",
            "Cesson Sévigné - Océane Consulting",
            "Rennes - Niji",
            "Saint-Grégoire - Autosur",
            "Laval - Soyooz",
            "Rennes - Atos",
            "Nantes - Capgemini"
    };

    //Animations variables
    private Animation animShow, animHide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offers_list_activity);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_menu);
        navigationView.setNavigationItemSelectedListener(this);

        populateAnnounces();

        // TODO : Check if animShow & animHide aren't useless
        //Prepare animations hide & show :
        animShow = AnimationUtils.loadAnimation(this, R.anim.view_show);
        animHide = AnimationUtils.loadAnimation(this, R.anim.view_hide);
    }

    // Methods : Generate Announces list :

    private void populateAnnounces(){
        offersListView = (ListView) findViewById(R.id.announces_listView);

        List<Offer> offers = generateOffers();

        OfferAdapter adapter = new OfferAdapter(OffersListActivity.this,offers);
        offersListView.setAdapter(adapter);

        //Link Announces to their detail
        showAnnouncesDetails();

    }

    private List<Offer> generateOffers(){
        List<Offer> offers = new ArrayList<Offer>();
        for(int i = 0; i<offersListTitles.length ; i++){
            offers.add(new Offer(offersListTitles[i], offersListDescriptions[i]));
        }
        return offers;
    }

    private void populateSpinners(){

        //Create 1 spinner per list
        Spinner competencesSpinner =(Spinner) findViewById(R.id.spinner_competences);
        Spinner localisationsSpinner =(Spinner) findViewById(R.id.spinner_localisations);
        Spinner diplomesSpinner =(Spinner) findViewById(R.id.spinner_diplomeas);

        //Create 1 adapter per list
        ArrayAdapter<CharSequence> competencesAdapter = ArrayAdapter.createFromResource(this,
                R.array.competences_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> localisationsAdapter = ArrayAdapter.createFromResource(this,
                R.array.localisations_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> diplomesAdapter = ArrayAdapter.createFromResource(this,
                R.array.diplomes_list, android.R.layout.simple_spinner_item);

        //Create 1 dropdownview per list
        competencesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        competencesSpinner.setAdapter(competencesAdapter);

        localisationsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        localisationsSpinner.setAdapter(localisationsAdapter);

        diplomesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        diplomesSpinner.setAdapter(diplomesAdapter);
    }

    // Methods : Openning other Activities :

    public void openFilters(View view){
        //Filter button action : calling filter activity
        //Intent intentFilter = new Intent(this,OffersFilterActivity.class);
        //startActivity(intentFilter);
        //overridePendingTransition(R.anim.view_show, R.anim.view_hide);
        setContentView(R.layout.offers_filter_activity);
        populateSpinners();
    }

    public void closeFilters(View view){
        //Filter button action : calling filter activity
        setContentView(R.layout.offers_list_activity);
        populateAnnounces();
    }

    private void showAnnouncesDetails(){

        offersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent myIntent = new Intent(OffersListActivity.this,OfferDetailsActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
