package com.pascogmail.mathieu.speedjobs.offers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pascogmail.mathieu.speedjobs.R;

import java.util.List;

/**
 * Created by Thomas Jacqueminet on 30/10/2015.
 */

public class OfferAdapter extends ArrayAdapter<Offer>{

    public OfferAdapter(Context context, List<Offer> offers){
        super(context,0,offers);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.offers_row,parent, false);
        }

        OfferViewHolder viewHolder = (OfferViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new OfferViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.description);
            convertView.setTag(viewHolder);
        }

        Offer offer = getItem(position);

        viewHolder.title.setText(offer.getTitle());
        viewHolder.description.setText(offer.getDescription());

        return convertView;
    }

    private class OfferViewHolder {
        public TextView title;
        public TextView description;
    }

}
