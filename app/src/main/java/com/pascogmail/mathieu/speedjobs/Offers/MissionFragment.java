package com.pascogmail.mathieu.speedjobs.offers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pascogmail.mathieu.speedjobs.R;

/**
 * Created by Thomas Jacqueminet on 04/11/2015.
 */
public class MissionFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.offers_tab_mission, container, false);
    }
}

