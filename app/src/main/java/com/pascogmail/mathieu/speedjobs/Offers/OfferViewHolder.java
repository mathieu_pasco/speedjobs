package com.pascogmail.mathieu.speedjobs.offers;

import android.widget.TextView;

/**
 * Created by Thomas Jacqueminet on 30/10/2015.
 */
public class OfferViewHolder {
    public TextView title;
    public TextView description;
}
