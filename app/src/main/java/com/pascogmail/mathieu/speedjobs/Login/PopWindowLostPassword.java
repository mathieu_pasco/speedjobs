package com.pascogmail.mathieu.speedjobs.login;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.pascogmail.mathieu.speedjobs.R;

/**
 * Created by mathi on 08/11/2015.
 */
public class PopWindowLostPassword extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_lost_password);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * 0.8), (int) (height * 0.4));
    }
}
