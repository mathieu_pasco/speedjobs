package com.pascogmail.mathieu.speedjobs.offers;

/**
 * Created by Thomas Jacqueminet on 30/10/2015.
 */
public class Offer {
    private String title;
    private String description;

    public Offer(String title, String description){
        this.title = title;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
