package com.pascogmail.mathieu.speedjobs.students;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import com.pascogmail.mathieu.speedjobs.R;


/**
 * Created by Thomas Jacqueminet on 30/10/2015.
 */

public class StudentAdapter extends ArrayAdapter<Student>{

    public StudentAdapter(Context context, List<Student> students){
        super(context,0,students);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.offers_row,parent, false);
        }

        OfferViewHolder viewHolder = (OfferViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new OfferViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.description);
            convertView.setTag(viewHolder);
        }

        Student student = getItem(position);

        viewHolder.title.setText(student.getFirstName() + " " + student.getLastName());
        viewHolder.description.setText(student.getDiplome() + " - " + student.getLocalisation());

        return convertView;
    }

    private class OfferViewHolder {
        public TextView title;
        public TextView description;
    }

}
