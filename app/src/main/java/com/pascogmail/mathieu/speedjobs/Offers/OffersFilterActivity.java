package com.pascogmail.mathieu.speedjobs.offers;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.pascogmail.mathieu.speedjobs.MyProfileActivity;
import com.pascogmail.mathieu.speedjobs.R;
import com.pascogmail.mathieu.speedjobs.students.StudentsListingActivity;
import com.pascogmail.mathieu.speedjobs.WelcomeActivity;

/**
 * Created by Thomas Jacqcueminet on 02/11/2015.
 */
public class OffersFilterActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.offers_filter_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_menu);
        navigationView.setNavigationItemSelectedListener(this);

        populateSpinners();
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            case R.id.offers_item_menu:
                Intent offer_page = new Intent(this, OffersListActivity.class);
                this.startActivity(offer_page);
                break;
            case R.id.welcome_item_menu:
                Intent welcome_page = new Intent(this, WelcomeActivity.class);
                this.startActivity(welcome_page);
                break;
            case R.id.my_profile_item_menu:
                Intent my_profile_page = new Intent(this, MyProfileActivity.class);
                this.startActivity(my_profile_page);
                break;
            case R.id.students_item_menu:
                Intent student_page = new Intent(this, StudentsListingActivity.class);
                this.startActivity(student_page);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void populateSpinners(){

        //Create 1 spinner per list
        Spinner competencesSpinner =(Spinner) findViewById(R.id.spinner_competences);
        Spinner localisationsSpinner =(Spinner) findViewById(R.id.spinner_localisations);
        Spinner diplomesSpinner =(Spinner) findViewById(R.id.spinner_diplomeas);

        //Create 1 adapter per list
        ArrayAdapter<CharSequence> competencesAdapter = ArrayAdapter.createFromResource(this,
                R.array.competences_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> localisationsAdapter = ArrayAdapter.createFromResource(this,
                R.array.localisations_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> diplomesAdapter = ArrayAdapter.createFromResource(this,
                R.array.diplomes_list, android.R.layout.simple_spinner_item);

        //Create 1 dropdownview per list
        competencesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        competencesSpinner.setAdapter(competencesAdapter);

        localisationsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        localisationsSpinner.setAdapter(localisationsAdapter);

        diplomesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        diplomesSpinner.setAdapter(diplomesAdapter);

    }

    public void showOnlyAnnounces(View view){
        //Filter button action : calling filter activity
        Intent intentFilter = new Intent(this,OffersListActivity.class);
        startActivity(intentFilter);
    }
}
