package com.pascogmail.mathieu.speedjobs.students;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.pascogmail.mathieu.speedjobs.R;

import java.util.List;
import java.util.Vector;

/**
 * Created by Remi on 09/12/2015.
 */
public class StudentDetailsActivity extends FragmentActivity {

    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.tab_asked_skills);
        setContentView(R.layout.students_details_activity);
    }
}
