package com.pascogmail.mathieu.speedjobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.pascogmail.mathieu.speedjobs.offers.OffersListActivity;
import com.pascogmail.mathieu.speedjobs.students.StudentsListingActivity;

public class SideMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.side_menu_activity);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id) {
            case R.id.offers_item_menu:
                Intent offer_page = new Intent(this, OffersListActivity.class);
                this.startActivity(offer_page);
                break;
            case R.id.welcome_item_menu:
                Intent welcome_page = new Intent(this, WelcomeActivity.class);
                this.startActivity(welcome_page);
                break;
            case R.id.my_profile_item_menu:
                Intent my_profile_page = new Intent(this, MyProfileActivity.class);
                this.startActivity(my_profile_page);
                break;
            case R.id.students_item_menu:
                Intent student_page = new Intent(this, StudentsListingActivity.class);
                this.startActivity(student_page);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
