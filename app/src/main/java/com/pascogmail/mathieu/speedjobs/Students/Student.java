package com.pascogmail.mathieu.speedjobs.students;

import java.util.Date;

/**
 * Created by Remi on 04/11/2015.
 */
public class Student {
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String diplome;
    private String localisation;

    public Student(String firstName, String lastName, String diplome, String localisation){
        this.firstName = firstName;
        this.lastName = lastName;
        this.diplome = diplome;
        this.localisation = localisation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getDiplome() {
        return diplome;
    }

    public void setDiplome(String diplome) {
        this.diplome = diplome;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

}
