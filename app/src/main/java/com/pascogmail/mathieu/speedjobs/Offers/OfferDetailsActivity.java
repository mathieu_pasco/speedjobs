package com.pascogmail.mathieu.speedjobs.offers;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.pascogmail.mathieu.speedjobs.R;

import java.util.List;
import java.util.Vector;

/**
 * Created by Thomas Jacqueminet on 04/11/2015.
 */
public class OfferDetailsActivity extends FragmentActivity {

    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //setContentView(R.layout.tab_asked_skills);
        setContentView(R.layout.offers_details_activity);

        // Création de la liste de Fragments que fera défiler le PagerAdapter
        List fragments = new Vector();

        // Ajout des Fragments dans la liste
        fragments.add(Fragment.instantiate(this,SkillsFragment.class.getName()));
        fragments.add(Fragment.instantiate(this,MissionFragment.class.getName()));
        fragments.add(Fragment.instantiate(this,OfferInformationsFragment.class.getName()));

        // Création de l'adapter qui s'occupera de l'affichage de la liste de
        // Fragments
        this.mPagerAdapter = new OffersDetailsAdapter(super.getSupportFragmentManager(), fragments);

        ViewPager pager = (ViewPager) super.findViewById(R.id.offers_details_viewpager1);
        // Affectation de l'adapter au ViewPager
        pager.setAdapter(this.mPagerAdapter);
    }
}
