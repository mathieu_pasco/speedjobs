package com.pascogmail.mathieu.speedjobs.students;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.pascogmail.mathieu.speedjobs.R;
import com.pascogmail.mathieu.speedjobs.SideMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Remi on 04/11/2015.
 */
public class StudentsListingActivity extends SideMenu {

    private StudentAdapter mAdapter;
    private ListView studentsListView;
    private String[] studentFirstName = {
            "Rémi",
            "Thomas",
            "Mathieu",
            "Julien",
            "Pierre",
            "Paul",
            "Jacques"
    };

    private String[] studentLastName = {
            "Zanzi",
            "Jacqueminet",
            "Pasco",
            "Martin",
            "Martin",
            "Martin",
            "Martin"
    };

    private String[] studentDiplome = {
            "CDPN M1",
            "CDPN M1",
            "CDPN M1",
            "BTS SIO",
            "DUT INFO",
            "DUT INFO",
            "LP"
    };

    private String[] studentLocalisation = {
            "Bruz",
            "Rennes",
            "Rennes",
            "Rennes",
            "Laval",
            "Laval",
            "Laval",
    };

    //Animations variables
    private Animation animShow, animHide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.students_listing_activity);
        populateAnnounces();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_menu);
        navigationView.setNavigationItemSelectedListener(this);


        // TODO : Check if animShow & animHide aren't useless
        //Prepare animations hide & show :
        animShow = AnimationUtils.loadAnimation(this, R.anim.view_show);
        animHide = AnimationUtils.loadAnimation(this, R.anim.view_hide);


    }

    // Methods : Generate Announces list :

    private void populateAnnounces() {
        studentsListView = (ListView) findViewById(R.id.announces_listView);

        List<Student> students = generateStudents();

        StudentAdapter adapter = new StudentAdapter(StudentsListingActivity.this, students);
        studentsListView.setAdapter(adapter);

        //Link Announces to their detail
        showAnnouncesDetails();

    }

    private List<Student> generateStudents() {
        List<Student> students = new ArrayList<Student>();
        for (int i = 0; i < studentFirstName.length; i++) {
            students.add(new Student(studentFirstName[i], studentLastName[i], studentDiplome[i], studentLocalisation[i]));
        }
        return students;
    }

    private void populateSpinners() {

        //Create 1 spinner per list
        Spinner competencesSpinner = (Spinner) findViewById(R.id.spinner_competences);
        Spinner localisationsSpinner = (Spinner) findViewById(R.id.spinner_localisations);
        Spinner diplomesSpinner = (Spinner) findViewById(R.id.spinner_diplomeas);

        //Create 1 adapter per list
        ArrayAdapter<CharSequence> competencesAdapter = ArrayAdapter.createFromResource(this,
                R.array.competences_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> localisationsAdapter = ArrayAdapter.createFromResource(this,
                R.array.localisations_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> diplomesAdapter = ArrayAdapter.createFromResource(this,
                R.array.diplomes_list, android.R.layout.simple_spinner_item);

        //Create 1 dropdownview per list
        competencesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        competencesSpinner.setAdapter(competencesAdapter);

        localisationsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        localisationsSpinner.setAdapter(localisationsAdapter);

        diplomesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        diplomesSpinner.setAdapter(diplomesAdapter);
    }

    // Methods : Openning other Activities :

    public void openFilters(View view) {
        //Filter button action : calling filter activity
        //Intent intentFilter = new Intent(this,StudentsFilterActivity.class);
        //startActivity(intentFilter);
        //overridePendingTransition(R.anim.view_show, R.anim.view_hide);
        setContentView(R.layout.offers_filter_activity);
        populateSpinners();
    }

    public void closeFilters(View view) {
        //Filter button action : calling filter activity
        setContentView(R.layout.offers_list_activity);
        populateAnnounces();
    }

    private void showAnnouncesDetails() {

        studentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent myIntent = new Intent(StudentsListingActivity.this, StudentDetailsActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
